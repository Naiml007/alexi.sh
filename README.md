# alexi.sh

Code for my portfolio and blog built with SvelteKit.

## License

The code for this project is licensed under the GPLv3 license (see [LICENSE](LICENSE)) except the following directories and their contents that are shared under the CC-BY-4.0 license:

- `static/img` — Media files are shared under the CC-BY-4.0 license.
- `content` — Text content for blog posts and projects are shared under the CC-BY-4.0 license.

See [content/LICENSE](content/LICENSE) and [static/img/LICENSE](static/img/LICENSE) for more information.
